package main

import (
	adapter2 "adapter/adapter"
	"fmt"
)

func main() {
	fmt.Println("*** Example Adapter ***")
	client := &adapter2.Client{}

	fetchAdapter := &adapter2.FetchAdapter{
		Instance: &adapter2.Fetch{},
	}
	client.Get(fetchAdapter, "https://www.google.com")

	axiosAdapter := &adapter2.AxiosAdapter{
		Instance: &adapter2.Axios{},
	}
	client.Get(axiosAdapter, "https://www.bornhup.com")

	fmt.Print("*** End of Adapter ***\n\n\n")
}
